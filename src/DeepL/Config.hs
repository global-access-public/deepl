{-# LANGUAGE DeriveDataTypeable #-}

module DeepL.Config where

import Data.Data (Data)
import Protolude (Text, Typeable)

type Token = Text

data Config = Config
  { token :: Token
  , input :: FilePath
  , output :: FilePath
  , lang :: Text
  }
  deriving (Show, Data, Typeable)
