{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-cse #-}

module Main where

import DeepL.Config
import DeepL.Translate (translateFile)
import Protolude
import System.Console.CmdArgs
  ( Data
  , Default (def)
  , cmdArgs
  , details
  , help
  , summary
  , typ
  , (&=)
  )
import Prelude hiding (print)

config :: Config
config =
  Config
    { token = "" &= help "deepL token" &= typ "abc123abc123"
    , input = def &= help "file to translate from" &= typ "input.txt"
    , output = def &= help "file to translate to" &= typ "output.txt"
    , lang = "" &= help "language to translate to" &= typ "EN"
    }
    &= help "deepl translator"
    &= summary "deepl-0.0.1, (C) Global Access 2021"
    &= details
      [ "deepl translates your files using deepl service"
      , ""
      , "You need a deepl token, get options with"
      , "  deepl --help"
      ]

main :: IO ()
main = cmdArgs config >>= translateFile
